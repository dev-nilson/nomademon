import mongoose from "mongoose";

const userSchema = new mongoose.Schema({
  id: { type: String, required: true },
  username: { type: String, required: true, unique: true },
  name: { type: String, required: true },
  image: String,
  bio: String,
  onboarded: {
    type: Boolean,
    default: false,
  },
  posts: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Post",
    },
  ],
  gangs: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Gang",
    },
  ],
});

const User = mongoose.models.User || mongoose.model("User", userSchema);
export default User;
