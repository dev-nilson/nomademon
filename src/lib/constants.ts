import {
  HomeIcon,
  MagnifyingGlassIcon,
  HeartIcon,
  PlusCircleIcon,
  UserGroupIcon,
  UserIcon,
} from "@heroicons/react/24/outline";

export const sidebarLinks = [
  {
    route: "/",
    label: "Home",
    icon: HomeIcon,
  },
  {
    route: "/search",
    label: "Search",
    icon: MagnifyingGlassIcon,
  },
  {
    route: "/activity",
    label: "Activity",
    icon: HeartIcon,
  },
  {
    route: "/create",
    label: "Create",
    icon: PlusCircleIcon,
  },
  {
    route: "/communities",
    label: "Communities",
    icon: UserGroupIcon,
  },
  {
    route: "/profile",
    label: "Profile",
    icon: UserIcon,
  },
];
