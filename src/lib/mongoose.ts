import mongoose, { mongo } from "mongoose";

let isConnected = false;

export const connectToDatabase = async () => {
  mongoose.set("strictQuery", true);

  if (!process.env.MONGO_URL) return console.log("Mongo not found");
  if (isConnected) return console.log("Connected to Mongo");

  try {
    await mongoose.connect(process.env.MONGO_URL);
    isConnected = true;
  } catch (error) {
    console.log(error);
  }
};
