import * as z from "zod";

export const PostValidation = z.object({
  post: z.string().nonempty().min(3).max(560),
  accountId: z.string(),
});

export const CommentValidation = z.object({
  post: z.string().nonempty().min(3).max(560),
});
