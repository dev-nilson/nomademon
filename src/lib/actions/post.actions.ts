import { revalidatePath } from "next/cache";
import Post from "../models/post.model";
import User from "../models/user.model";
import { connectToDatabase } from "../mongoose";

type CreatePostParams = {
  text: string;
  author: string;
  gangId: string | null;
  path: string;
};

export async function createPost({
  text,
  author,
  gangId,
  path,
}: CreatePostParams) {
  try {
    connectToDatabase();

    const createdPost = await Post.create({
      text,
      author,
      gang: null,
    });

    await User.findById(author, {
      $push: {
        posts: createdPost._id,
      },
    });

    revalidatePath(path);
  } catch (error: any) {
    throw new Error("Failed to create post:", error);
  }
}
