"use client";
import { useRouter } from "next/navigation";
import { SignOutButton, SignedIn } from "@clerk/nextjs";
import Link from "next/link";

export default function Header() {
  const router = useRouter();

  return (
    <nav className="header">
      <Link href="/" className="flex items-center text-white gap-4">
        nomademon
      </Link>
      <div className="flex items-center gap-1">
        <div className="block md:hidden">
          <SignedIn>
            <SignOutButton signOutCallback={() => router.push("/login")}>
              <div className="flex cursor-pointer text-white">Sign Out</div>
            </SignOutButton>
          </SignedIn>
        </div>
      </div>
    </nav>
  );
}
