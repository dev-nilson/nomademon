"use client";
import { usePathname } from "next/navigation";
import { sidebarLinks } from "@/lib/constants";
import Link from "next/link";

export default function Footer() {
  const pathname = usePathname();

  return (
    <footer className="footer">
      <div className="footer-container">
        {sidebarLinks.map((link) => {
          const Icon = link.icon;
          const isActive =
            (pathname.includes(link.route) && link.route.length > 1) ||
            pathname === link.route;

          return (
            <Link
              href={link.route}
              className={`footer-link ${isActive && "bg-primary-500"}`}
              key={link.label}
            >
              <Icon color="#ffffff" height={24} width={24} />
              <p className="text-subtle-medium text-light-1 max-sm:hidden">
                {link.label.split(/\s+/)[0]}
              </p>
            </Link>
          );
        })}
      </div>
    </footer>
  );
}
