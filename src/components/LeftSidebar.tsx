"use client";
import { usePathname, useRouter } from "next/navigation";
import { SignOutButton, SignedIn } from "@clerk/nextjs";
import { sidebarLinks } from "@/lib/constants";
import { ArrowLeftOnRectangleIcon } from "@heroicons/react/24/outline";
import Link from "next/link";

export default function LeftSidebar() {
  const pathname = usePathname();
  const router = useRouter();

  return (
    <section className="custom-scrollbar leftsidebar">
      <div className="flex w-full flex-1 flex-col gap-6 px-6">
        {sidebarLinks.map((link) => {
          const Icon = link.icon;
          const isActive =
            (pathname.includes(link.route) && link.route.length > 1) ||
            pathname === link.route;

          return (
            <Link
              href={link.route}
              className={`left-sidebar-link hover:bg-primary-500 ${isActive && "bg-primary-500"}`}
              key={link.label}
            >
              <Icon color="#ffffff" height={24} width={24} />
              <p className="text-light-1 max-lg:hidden">{link.label}</p>
            </Link>
          );
        })}
      </div>
      <div className="mt-10 px-6">
        <SignedIn>
          <SignOutButton signOutCallback={() => router.push("/login")}>
            <div className="left-sidebar-link cursor-pointer">
              <ArrowLeftOnRectangleIcon
                color="#ffffff"
                height={24}
                width={24}
              />
              <p className="text-light-1 max-lg:hidden">Sign Out</p>
            </div>
          </SignOutButton>
        </SignedIn>
      </div>
    </section>
  );
}
